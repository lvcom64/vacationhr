package com.example.vacationhr.service;

import com.example.vacationhr.rest.VacationRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class VacationsService {
    private static RestTemplate restTemplate = new RestTemplate();
    private static ObjectMapper objectMapper = new ObjectMapper();
    private  static HttpHeaders httpHeaders = new HttpHeaders();

    private  VacationsService() {throw new IllegalStateException("Utility class");}

    public  static String  registerVacation(VacationRequest vacationRequest) throws JsonProcessingException {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String registerVacationUrl = "http://localhost:8280/api/v3/vacations";
        ResponseEntity<String> response =  restTemplate.postForEntity(registerVacationUrl,vacationRequest,String.class);
        JsonNode root = objectMapper.readTree(response.getBody());
        return root.path("vacationId").asText();
    }

    public static String getVcations (VacationRequest vacationRequest) throws JsonProcessingException {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String getVacationsUrl = "http://localhost:8280/api/v3/vacation/{employeeId}";
        ResponseEntity<String> response =  restTemplate.getForEntity(getVacationsUrl ,String.class);
        JsonNode root = objectMapper.readTree(response.getBody());
        return root.path("vacations").asText();
    }

    public static Integer getVacationsByEmployeeIdLastYear(int employeeId) throws JsonProcessingException {
        String getVacationsByEmployeeIdLastYearUrl = "http://localhost:8280/api/v3/vacations/{employeeId}/employee/year";
        ResponseEntity<String> response = restTemplate.getForEntity(getVacationsByEmployeeIdLastYearUrl, String.class, employeeId);
        JsonNode root = objectMapper.readTree(response.getBody());
        return root.path("headId").asInt();
    }


}

