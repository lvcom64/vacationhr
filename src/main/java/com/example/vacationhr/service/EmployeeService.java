package com.example.vacationhr.service;

import com.example.vacationhr.report.BeanEmployee;
import com.example.vacationhr.rest.EmployeeRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class EmployeeService {
    private static RestTemplate restTemplate = new RestTemplate();
    private static ObjectMapper objectMapper = new ObjectMapper();
    private  static HttpHeaders httpHeaders = new HttpHeaders();

    private EmployeeService() {throw new IllegalStateException("Utility class");}
    public  static String  registerEmployee(EmployeeRequest employeeRequest) throws JsonProcessingException {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String registerEmployeeUrl = "http://localhost:8280/api/v1/employees";
        ResponseEntity<String> response =  restTemplate.postForEntity(registerEmployeeUrl,employeeRequest,String.class);
        JsonNode root = objectMapper.readTree(response.getBody());
        return root.path("employeeId").asText();
    }

    public static BeanEmployee getEmployee (String employeeId)  {
        String getEmployeeUrl = "http://localhost:8280/api/v1/employees/{employeeId}";
        ResponseEntity<BeanEmployee> response =  restTemplate.getForEntity(getEmployeeUrl, BeanEmployee.class,employeeId);
        return response.getBody();

    }

}
