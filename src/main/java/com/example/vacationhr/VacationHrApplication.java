package com.example.vacationhr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VacationHrApplication {

    public static void main(String[] args) {
        SpringApplication.run(VacationHrApplication.class, args);
    }

}
