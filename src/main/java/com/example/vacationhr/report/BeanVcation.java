package com.example.vacationhr.report;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonCreator;

public class BeanVcation {

    private String employeeId;
    private String dateStart;
    private String dateEnd;
    private int type;

    @JsonCreator
    public BeanVcation(
            @JsonProperty("employeeId")
                    String employeeId,
            @JsonProperty("dateStart")
                    String dateStart,
            @JsonProperty("dateEnd")
                    String dateEnd,
            @JsonProperty("type")
                    int type
    ){ this.employeeId = employeeId;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.type = type;}





    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


}
