package com.example.vacationhr.report;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

public class BeanEmployee {

    private String employeeId;
    private String familyName;
    private String firstName;
    private String secondName;
    private String unit;
    private String position;
    private int salary;
    private String hired;
    @Nullable
    private String fired;

    @JsonCreator
    public BeanEmployee(
            @JsonProperty("employeeId")
                    String employeeId,
            @JsonProperty("familyName")
                    String familyName,
            @JsonProperty("firstName")
                    String firstName,
            @JsonProperty("secondName")
                    String secondName,
            @JsonProperty("unit")
                    String unit,
            @JsonProperty("position")
                    String position,
            @JsonProperty("salary")
                    int salary,
            @JsonProperty("hired")
                    String hired,
            @JsonProperty("fired")
            @Nullable String fired) {
        this.employeeId = employeeId;
        this.familyName = familyName;
        this.firstName = firstName;
        this.secondName = secondName;
        this.unit = unit;
        this.position = position;
        this.salary = salary;
        this.hired = hired;
        this.fired = fired;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getHired() {
        return hired;
    }

    public void setHired(String hired) {
        this.hired = hired;
    }

    @Nullable
    public String getFired() {
        return fired;
    }

    public void setFired(@Nullable String fired) {
        this.fired = fired;
    }
}