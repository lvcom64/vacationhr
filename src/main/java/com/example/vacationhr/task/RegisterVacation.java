package com.example.vacationhr.task;

import com.example.vacationhr.rest.VacationRequest;
import com.example.vacationhr.service.VacationsService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Component("RegisterVacation")
public class RegisterVacation implements JavaDelegate {


    @Override
    public void execute(DelegateExecution execution) throws Exception {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        VacationRequest vacationRequest = new VacationRequest(
                (int) execution.getVariable("employeeId"),
                dateFormat.format(execution.getVariable("dateStart")),
                dateFormat.format(execution.getVariable("dateEnd")),
                (int) execution.getVariable("type")
        );
        String vacationId = VacationsService.registerVacation(vacationRequest);
        execution.setVariable("vacationId", vacationId );
    }
}
