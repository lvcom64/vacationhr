package com.example.vacationhr.task;

import com.example.vacationhr.rest.VacationRequest;
import com.example.vacationhr.service.VacationsService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Component("GetVacationDays")
public class GetVacationDays implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {

        Integer days = VacationsService.getVacationsByEmployeeIdLastYear((int) execution.getVariable("employeeId"));

        execution.setVariable("days", days);
    }
}
