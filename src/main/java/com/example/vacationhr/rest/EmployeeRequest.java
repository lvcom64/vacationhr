package com.example.vacationhr.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeRequest {
    private String familyName;
    private String firstName;
    private String secondName;
    private int unitId;
    private int positionId;
    private int salary;
    private String dateHired;
}
