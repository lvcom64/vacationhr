package com.example.vacationhr.rest;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VacationRequest {
    private int employeeId;
    private String dateStart;
    private String dateEnd;
    private int type;
}
